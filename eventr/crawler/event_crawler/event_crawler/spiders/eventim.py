import scrapy
from event_crawler.items import EventItem
from urllib.parse import urlparse
from datetime import datetime
import csv
import re


class EventimSpider(scrapy.Spider):
    name = 'eventim'
    allowed_domains = ['eventim.bg']
    start_urls = ['http://eventim.bg/']

    def __init__(self, performer=None, file_path=None):
        self.performer = performer
        self.file_path = file_path
        self.base_start_path = "http://www.eventim.bg/bg/tursi/?search_string="

    def start_requests(self):
        urls = []

        # replace the input string's " " with "+"
        def convert_input_to_acceptable_form(input_str):
            return input_str.replace(" ", "+")

        def extract_input_file(input_path):
            with open(input_path) as f:
                data = csv.reader(f, delimiter="\n")
                for row in data:
                    artist = row[0]
                    artist = self.base_start_path + \
                        convert_input_to_acceptable_form(artist)
                    urls.append(artist)

        # TODO
        # set urls from input file
        if self.file_path:
            extract_input_file(self.file_path)

        # set performer from input
        if self.performer:
            self.performer = self.base_start_path + \
                convert_input_to_acceptable_form(self.performer)
            urls.append(self.performer)

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        base_path = "https://www.eventim.bg/bg/tursi/\?search_string="
        author_name = re.split(base_path, response.url)
        author_name = author_name[1]
        author_name = author_name.replace("+", " ")

        # hardcode edge case handler variable
        event_name_edge_results = [' Сортирай резултатите ', ]

        event_names = response.xpath(
            "//h3/text()"
        ).extract()
        
        for edge in event_name_edge_results:
            if(edge in event_names):
                event_names.remove(edge)


        event_dates = response.xpath(
            "//span[@class='Badge-day']/text()"
        ).extract()
        event_months = response.xpath(
            "//span[@class='Badge-month']/text()"
        ).extract()

        # handle city
        event_cities = response.xpath(
            "//address/span[@itemprop='addressLocality']/text()"
        ).extract()

        # handle country
        event_countries = response.xpath(
            "//address/meta[@itemprop='addressCountry']/@content"
        ).extract()

        print(event_countries)
        print("*"*50)

        # scrape all matches
        if(event_names != []):
            for event_name, event_date, event_month, event_city, event_country in zip(event_names,
                                                                                      event_dates,
                                                                                      event_months,
                                                                                      event_cities,
                                                                                      event_countries,
                                                                                      ):

                # event date                                                           
                event_output_date = event_date + " " + event_month

                yield EventItem(event_name=event_name,
                                artist_name=author_name,
                                event_date=event_output_date,
                                event_link=response.url,
                                event_city=event_city,
                                event_country=event_country,
                                )

            # artists_node_value = '\n                Artists\n            '
            # artists = response.xpath(
            #     "//section[h2/a/text()='"+artists_node_value+"']/ol/li/div/a/@href"
            # )
            # site_url = "https://www.last.fm"
            # address_to_events_page = "/+events"
            # for artist in artists:
            #     address_to_artist = artist.extract()
            #     url_to_artist = site_url+address_to_artist+address_to_events_page
            #     yield scrapy.Request(url=url_to_artist, callback=self.parse_events)
