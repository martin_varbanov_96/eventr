import scrapy
from event_crawler.items import EventItem
from urllib.parse import urlparse
from datetime import datetime
import csv
import re


class BiletBgSpider(scrapy.Spider):
    name = 'bilet_bg'
    allowed_domains = ['bilet.bg']
    start_urls = ['http://bilet.bg/']

    def __init__(self, performer=None, file_path=None):
        self.performer = performer
        self.file_path = file_path
        self.base_start_path = "https://bilet.bg/bg/search/results/1?keyword="
    
    def start_requests(self):
        urls = []

        # replace the input string's " " with "+"
        def convert_input_to_acceptable_form(input_str):

            # WARNING: %20 could be errorprone
            return input_str.replace(" ", "%20")

        def extract_input_file(input_path):
            with open(input_path) as f:
                data = csv.reader(f, delimiter="\n")
                for row in data:
                    artist = row[0]
                    artist = self.base_start_path + \
                        convert_input_to_acceptable_form(artist)
                    urls.append(artist)

        # TODO
        # set urls from input file
        if self.file_path:
            extract_input_file(self.file_path)

        # set performer from input
        if self.performer:
            self.performer = self.base_start_path + \
                convert_input_to_acceptable_form(self.performer)
            urls.append(self.performer)

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        base_path = "https://bilet.bg/bg/search/results/1?keyword="
        author_name = response.url.replace(base_path, "")
        author_name = author_name.replace("%20", " ")
        
        event_names = response.xpath(
            "//h3/text()"
            ).extract()

        edge_cases = ['Допънителна информация',
                      'SOCIAL LINKS',
                      'Partners',
                      'Bilet.bg табло',
                      'Помощ и Контакт',
                      ]

        # TODO: RESOLVE n^3
        for i, name in enumerate(event_names):
            if(str(name) in edge_cases):
                del event_names[i]
        print(event_names)
        print("*"*50)
        
        for i, name in enumerate(event_names):
            if(name == 'Partners'):
                event_names.remove(name)

        for i, name in enumerate(event_names):        
            if(name == 'Помощ и Контакт'):
                event_names.remove(name)
        
        event_dates = response.xpath(
            "//div[@class='search-result-item-info col-md-6 col-sm-6 col-xs-12']//li[@class='col-sm-4 col-lg-4']/text()[preceding-sibling::span]"
        ).extract()

        event_cities = response.xpath(
            "//div[@class='search-result-item-info col-md-6 col-sm-6 col-xs-12']//li[@class='col-sm-5 col-lg-5']/text()[preceding-sibling::span]"
        ).extract()

        for i, el in enumerate(event_dates):
            event_dates[i] = event_dates[i].replace("\t", "")
            event_dates[i] = event_dates[i].replace("\r", "")
            event_dates[i] = event_dates[i].replace("\n", "")


        for i, el in enumerate(event_cities):
            event_cities[i] = event_cities[i].replace("\t", "")
            event_cities[i] = event_cities[i].replace("\r", "")
            event_cities[i] = event_cities[i].replace("\n", "")


        
        # print(event_dates)
        # print("*"*30)


        if(event_names != []):
            for event_name, event_date, event_city in zip(event_names,
                                                            event_dates,
                                                            event_cities,
                                                            ):
                yield EventItem(event_link=response.url,
                                event_name=event_name,
                                event_date=event_date,
                                artist_name=author_name,
                                event_city=event_city,
                                )        

        # print(event_dates)
        # print("*"*60)





        # edge_cases = ['Помощ и Контакт', 'Partners']
        # for name in event_names:
        #     print()
        #     if(name in edge_cases):
        #         event_names.remove(name)
        # print(event_names)
        # print(edge_cases[0] in event_names)
        # print("*"*50)
