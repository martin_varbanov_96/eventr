# -*- coding: utf-8 -*-
import scrapy
from event_crawler.items import AlbumItem
import csv

class AlbumspiderSpider(scrapy.Spider):
    name = 'AlbumSpider'
    allowed_domains = ['last.fm']
    start_urls = ['http://last.fm/']

    def __init__(self, performer=None, file_path=None):
        self.performer = performer
        self.file_path = file_path
        self.base_start_path = "https://www.last.fm/search?q="
    
    def start_requests(self):
        urls = []
        
        #replace the input string's " " with "+" 
        def convert_input_to_acceptable_form(input_str):
            return input_str.replace(" ", "+")

        def extract_input_file(input_path):
            with open(input_path) as f:
                data = csv.reader(f, delimiter="\n")
                for row in data:
                    artist = row[0]
                    artist = self.base_start_path + \
                        convert_input_to_acceptable_form(artist)
                    urls.append(artist)
                    
        # TODO
        # set urls from input file
        if self.file_path:
            extract_input_file(self.file_path)

        # set performer from input
        if self.performer:
            self.performer = self.base_start_path + \
                convert_input_to_acceptable_form(self.performer)
            urls.append(self.performer)
            
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

        
    def parse(self, response):
        artists_node_value = '\n                Artists\n            '
        artists = response.xpath(
            "//section[h2/a/text()='"+artists_node_value+"']/ol/li/div/a/@href"
        )
        site_url = "https://www.last.fm"
        address_to_events_page = "/+albums"
        for artist in artists:
            print("*"*50)
            print(artits)
            print("*"*50)
        
        yield AlbumItem(album_artist="TEST")