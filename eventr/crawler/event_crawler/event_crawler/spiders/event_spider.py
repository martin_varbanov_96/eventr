import scrapy
from event_crawler.items import EventItem
from urllib.parse import urlparse
from datetime import datetime
import csv
import re


class EventSpider(scrapy.Spider):
    name = "event_spider"

    def __init__(self, performer=None, file_path=None):
        self.performer = performer
        self.file_path = file_path
        self.base_start_path = "https://www.last.fm/search?q="

    def start_requests(self):
        urls = []

        # replace the input string's " " with "+" 
        def convert_input_to_acceptable_form(input_str):
            return input_str.replace(" ", "+")

        def extract_input_file(input_path):
            with open(input_path) as f:
                data = csv.reader(f, delimiter="\n")
                for row in data:
                    artist = row[0]
                    artist = self.base_start_path + \
                        convert_input_to_acceptable_form(artist)
                    urls.append(artist)

        # TODO
        # set urls from input file
        if self.file_path:
            extract_input_file(self.file_path)

        # set performer from input
        if self.performer:
            self.performer = self.base_start_path + \
                convert_input_to_acceptable_form(self.performer)
            urls.append(self.performer)

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        artists_node_value = '\n                Artists\n            '
        artists = response.xpath(
            "//section[h2/a/text()='"+artists_node_value+"']/ol/li/div/a/@href"
        )
        site_url = "https://www.last.fm"
        address_to_events_page = "/+events"
        for artist in artists:
            address_to_artist = artist.extract()
            url_to_artist = site_url+address_to_artist+address_to_events_page
            yield scrapy.Request(url=url_to_artist, callback=self.parse_events)

    # scrape data from all of the events
    def parse_events(self, response):

        # scrape a list of cities of the events
        class_name_city = "events-list-item-venue--city"
        class_name_country = "events-list-item-venue--country"
        events_cities = response.xpath(
            "//div[@class='"+class_name_city+"']/text()").extract()

        # scape a list of countries of events
        events_countries = response.xpath(
            "//div[@class='"+class_name_country+"']/text()").extract()
        date_class_name = "events-list-item-art"

        # scrape a list of dates of the events
        event_dates = response.xpath(
            "//td[@class='"+date_class_name+"']/time/@datetime").extract()

        # scrape list of names of events
        # TODO:: Trim the unnecesary elements
        event_class_name = "events-list-item-event--title"
        event_names = response.xpath(
            "//div[@class='"+event_class_name+"']/a/text()").extract()

        for i in range(len(event_dates)):
            event_dates[i] = datetime.strptime(
                event_dates[i], '%Y-%m-%dT%H:%M:%S')

        # strip for link for artist name with regex
        base_path = "https://www.last.fm/music/"
        tail_path = "/\+events"
        author_name = re.split(base_path, response.url)
        author_name = re.split(tail_path, author_name[1])
        author_name = author_name[0]
        author_name = author_name.replace("+", " ")

        # for each datetime yield to another parse function to yield events for each date
        for i in range(len(event_dates)):
            # request_single_event=sc
            # request_within = scrapy.Request(link_to_next_page,
            #                                 self.parse_inner_page)
            # request_within.meta['link_id'] = self.link_id
            # request_within.meta['status'] = self.status
            # request_within.meta['site_id'] = self.site_id
            # yield request_within

            yield EventItem(event_name=event_names[i],
                            event_country=events_countries[i],
                            event_city=events_cities[i],
                            event_date=event_dates[i],
                            artist_name=author_name
                            )
