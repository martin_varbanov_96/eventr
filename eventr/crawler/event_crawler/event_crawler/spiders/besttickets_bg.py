import scrapy
from event_crawler.items import EventItem
from urllib.parse import urlparse
from datetime import datetime
import csv
import re

class BestticketsBgSpider(scrapy.Spider):
    name = 'besttickets_bg'
    allowed_domains = ['besttickets.bg']
    start_urls = ['http://besttickets.bg/']

    def __init__(self, performer=None, file_path=None):
        self.performer = performer
        self.file_path = file_path
        self.base_start_path = "https://besttickets.bg/search?body_value={}&between_date_filter%5Bvalue%5D%5Bdate%5D=&field_event_type_tid=All&field_location_tid=All"

    def start_requests(self):
        urls = []
        # replace the input string's " " with "+"
        def convert_input_to_acceptable_form(input_str):

            # WARNING: %20 could be errorprone
            return input_str.replace(" ", "+")

        def extract_input_file(input_path):
            with open(input_path) as f:
                data = csv.reader(f, delimiter="\n")
                for row in data:
                    artist = row[0]
                    artist_urled_name = convert_input_to_acceptable_form(artist)
                    artist = self.base_start_path.format(artist_urled_name)
                        
                    urls.append(artist)

        # set urls from input file
        if self.file_path:
            extract_input_file(self.file_path)

        # set performer from input
        if self.performer:
            artist_urled_name = convert_input_to_acceptable_form(self.performer)
            self.performer = self.base_start_path.format(artist_urled_name)
            urls.append(self.performer)

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        artist_name = response.url.replace("https://besttickets.bg/search?body_value=", "")
        artist_name = artist_name.replace("&between_date_filter%5Bvalue%5D%5Bdate%5D=&field_event_type_tid=All&field_location_tid=All", "")

        event_names = response.xpath(
            "//div[@class='field field--name-title-field field--type-text field--label-hidden']//div[@class='field__item even']/text()"
        ).extract()

        event_dates = response.xpath(
            "//span[@class='date-display-single']/@content"
        ).extract()

        event_cities = response.xpath(
            "//div[@class='field__item even']/a/text()"
        ).extract()
        print(event_cities)
        print("*"*300)

        if(event_names != []):
            for name, date, city in zip(event_names,
                                  event_dates,
                                  event_cities,
                                  ):
                yield EventItem(event_link=response.url,
                                event_name=name,
                                event_date=date,
                                artist_name=artist_name,
                                event_city=city,
                                )       