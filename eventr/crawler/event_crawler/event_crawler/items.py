# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class EventItem(scrapy.Item):
    event_name = scrapy.Field()
    event_city = scrapy.Field()
    event_country = scrapy.Field()
    event_date = scrapy.Field()
    artist_name = scrapy.Field()
    event_link = scrapy.Field()

class AlbumItem(scrapy.Item):
    album_artist = scrapy.Field()
    album_name = scrapy.Field()
    album_release_date = scrapy.Field()
    songs = scrapy.Field()